package monme;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Place {
    private final String identifier;
    private final String parentIdentifier;
    private final List<String> ancestors;

    Place(final String identifier, final String parentIdentifier, final String... ancestors) {
        assert identifier != null;
        assert ancestors != null;

        this.identifier = identifier;
        this.parentIdentifier = parentIdentifier;
        this.ancestors = Collections.unmodifiableList(Arrays.asList(ancestors));
    }

    public Place withAncestors(List<String> ancestors) {
        String[] a = ancestors.toArray(new String[ancestors.size()]);
        return new Place(this.identifier, this.parentIdentifier, a);
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getParentIdentifier() {
        return parentIdentifier;
    }

    public List<String> getAncestors() {
        return ancestors;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        Place rhs = (Place) obj;

        return new EqualsBuilder()
                .append(identifier, rhs.identifier)
                .append(parentIdentifier, rhs.parentIdentifier)
                .append(ancestors, rhs.ancestors)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(13, 37)
                .append(identifier)
                .append(parentIdentifier)
                .append(ancestors)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("identifier", identifier)
                .append("parentIdentifier", parentIdentifier)
                .append("ancestors", ancestors)
                .toString();
    }
}
