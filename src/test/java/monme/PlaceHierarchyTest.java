package monme;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class PlaceHierarchyTest {

    @Test
    public void testSinglePlace() {
        List<Place> places = Collections.singletonList(new Place("united-states", null));
        List<Place> expected = Collections.singletonList(new Place("united-states", null));

        List<Place> placesWithAncestors = PlaceHierarchy.build(places);
        assertNotSame("Should return a copy of the list", places, placesWithAncestors);
        assertEquals(expected, placesWithAncestors);
    }

    @Test
    public void testTwoPlaces() {
        List<Place> places = Arrays.asList(
                new Place("illinois-us", "united-states"),
                new Place("united-states", null)
        );
        List<Place> expected = Arrays.asList(
                new Place("united-states", null),
                new Place("illinois-us", "united-states", "united-states")
        );

        List<Place> placesWithAncestors = PlaceHierarchy.build(places);
        assertNotSame("Should return a copy of the list", places, placesWithAncestors);

        // We don't care what the sort order is when comparing
        placesWithAncestors.sort(Comparator.comparing(Place::getIdentifier));
        expected.sort(Comparator.comparing(Place::getIdentifier));

        assertEquals(expected, placesWithAncestors);
    }

    @Test
    public void testMorePlaces() {
        List<Place> places = Arrays.asList(
                new Place("chicago-illinois-us", "cook-county-illinois-us"),
                new Place("illinois-us", "united-states"),
                new Place("united-states", null),
                new Place("massachusetts-us", "united-states"),
                new Place("boston-massachusetts-us", "suffolk-county-massachusetts-us"),
                new Place("suffolk-county-massachusetts-us", "massachusetts-us"),
                new Place("cook-county-illinois-us", "illinois-us"),
                new Place("san-mateo-county-california-us", "california-us"),
                new Place("san-mateo-city-california-us", "san-mateo-county-california-us"),
                new Place("california-us", "united-states")
        );

        List<Place> expected = Arrays.asList(
                new Place("chicago-illinois-us", "cook-county-illinois-us","cook-county-illinois-us", "illinois-us", "united-states"),
                new Place("cook-county-illinois-us", "illinois-us", "illinois-us", "united-states"),
                new Place("illinois-us", "united-states", "united-states"),
                new Place("boston-massachusetts-us", "suffolk-county-massachusetts-us", "suffolk-county-massachusetts-us", "massachusetts-us", "united-states"),
                new Place("suffolk-county-massachusetts-us", "massachusetts-us", "massachusetts-us", "united-states"),
                new Place("massachusetts-us", "united-states", "united-states"),
                new Place("san-mateo-city-california-us", "san-mateo-county-california-us", "san-mateo-county-california-us", "california-us", "united-states"),
                new Place("san-mateo-county-california-us", "california-us", "california-us", "united-states"),
                new Place("california-us", "united-states", "united-states"),
                new Place("united-states", null)
        );

        List<Place> placesWithAncestors = PlaceHierarchy.build(places);
        assertNotSame("Should return a copy of the list", places, placesWithAncestors);

        // We don't care what the sort order is when comparing
        placesWithAncestors.sort(Comparator.comparing(Place::getIdentifier));
        expected.sort(Comparator.comparing(Place::getIdentifier));

        assertEquals(expected, placesWithAncestors);
    }
}
