# java-place-hierarchy

Java project for a coding exercise. The goal of the project is to build a list of the ancestors of a
place based on a list of places and their parent place.

For example, if

* City of Boston has a parent of Suffolk County
* Suffolk County has a parent of Massachusetts
* Massachusetts has a parent of USA

We can determine that the ancestors of Boston are: Suffolk County, Massachusetts, USA.

### Instructions

1. Run the unit tests and observe that they all fail.
2. Implement the `build` function in the `PlaceHierarchy` class so that all the tests pass. Make
   sure you do not modify the test code in any way. The tests start off as simple and become more
   complex, so you could start off getting the first one to pass and proceed from there.

**Note** that the sort order of the list of places that are returned from `PlaceHierarchy.build`
does not matter, but the order of the ancestors does.
