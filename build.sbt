organization := "monme"
name := "java-place-hierarchy"
version := "1.0"

autoScalaLibrary := false
crossPaths := false

libraryDependencies ++= Seq(
  "org.apache.commons" % "commons-lang3" % "3.7",
  "junit" % "junit" % "4.12" % Test,
  "com.novocode" % "junit-interface" % "0.11" % Test
)
